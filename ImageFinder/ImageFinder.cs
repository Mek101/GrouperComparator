using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using GrouperComparator;

namespace ImageFinder
{
	public class ImageSet
	{
		public IList<MetaImage> Images { get; private set; }
		public MetaImage SelectedImage { get; private set; }


		public ImageSet(IList<MetaImage> images, int selectedImage)
		{
			Images = images;
			SelectedImage = images[selectedImage];
		}
	}


	public static class ImageFinder
	{
		public static Task<IList<ImageSet>> FindDuplicatesAsync(string path, Configuration config)
		{
			MetaImage metaImg = new MetaImage(path);
			return FindDuplicatesAsync(new MetaImage[]{ metaImg }, config);
		}

		public static Task<IList<ImageSet>> FindDuplicatesAsync(IEnumerable<string> paths, Configuration config)
		{
			return FindDuplicatesAsync(paths.Select((p) => new MetaImage(p)).ToArray(), config);
		}

		public static async Task<IList<ImageSet>> FindDuplicatesAsync(IReadOnlyList<MetaImage> paths, Configuration config)
		{
			RuleFilter<MetaImage> filter = new RuleFilter<MetaImage>(config.Rules);

			IList<List<MetaImage>> sets = await Comparator.GroupInSetsAsync(paths, config.Differentiator, config.Parallelization);

			List<ImageSet> resultinSets = new List<ImageSet>(sets.Count);
			Parallel.ForEach(sets, delegate(List<MetaImage> set)
			{
				List<MetaImage> images = filter.Apply(set).ToList();
				lock(resultinSets)
					resultinSets.Add(new ImageSet(images, 0));
			});

			return resultinSets;
		}
	}
}