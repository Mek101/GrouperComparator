using SixLabors.ImageSharp;

namespace ImageFinder
{
	public class MetaImage
	{
		public Image Image;

		public string Path;


		public MetaImage(string path)
		{
			Image = Image.Load(path);
			Path = path;
		}
	}
}