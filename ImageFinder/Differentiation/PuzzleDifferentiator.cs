﻿using System;
using System.Collections.Generic;
using Puzzle;
using SixLabors.ImageSharp;
using GrouperComparator.Differentiation;

namespace ImageFinder.Differentiation
{
	class PuzzleDifferentiator : IDifferentiator<Image, double>
	{
		private readonly double _margin;
		private readonly SignatureGenerator _generator;


		public PuzzleDifferentiator(double margin, uint gridSize = 9, double noiseCutoff = 2, double sampleSizeRatio = 2, bool enableAutocrop = true)
		{
			_margin = margin;
			_generator = new SignatureGenerator(gridSize, noiseCutoff, sampleSizeRatio, enableAutocrop);
		}

		public double Compare(Image elemSource, Image elemReference, out bool comparable)
		{
			IEnumerable<LuminosityLevel> sourceSignature = _generator.GenerateSignature(elemSource);
			IEnumerable<LuminosityLevel> referenceSignature = _generator.GenerateSignature(elemReference);

			double distance = Math.Abs(sourceSignature.NormalizedDistance(referenceSignature));

			comparable = (distance < _margin);
			return distance;
		}
	}
}
