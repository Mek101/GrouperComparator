using System.Linq;
using System.Collections.Generic;

namespace ImageFinder.Rules
{
	public class RuleByResolution : IRule<MetaImage>
	{
		public IEnumerable<MetaImage> Process(IEnumerable<MetaImage> objs)
		{
			return objs.OrderBy((m) => m.Image.Height * m.Image.Width);
		}
	}
}