using System;
using System.IO;
using System.Collections.Generic;

namespace ImageFinder.Rules
{
	public enum PathSelectionMode : byte
	{
		/// <summary>
		/// Judges negatively every path outside the given.
		/// </summary>
		Whitelist,
		/// <summary>
		/// Judges positively every path outside the given.
		/// </summary>
		Blacklist
	}

	public class PathInclusionRule : IRule<MetaImage>
	{
		private readonly string _referencePath;
		private readonly PathSelectionMode _mode;


		/// <summary>
		/// Source: https://stackoverflow.com/questions/5617320/given-full-path-check-if-path-is-subdirectory-of-some-other-path-or-otherwise/31941159#31941159
		/// Returns true if <paramref name="path"/> starts with the path <paramref name="baseDirPath"/>.
		/// The comparison is case-insensitive, handles / and \ slashes as folder separators and
		/// only matches if the base dir folder name is matched exactly ("c:\foobar\file.txt" is not a sub path of "c:\foo").
		/// </summary>
		private bool IsSubPathOf(string path, string baseDirPath)
		{
			string normalizedPath = Path.GetFullPath(WithEnding(path.Replace('/', '\\'), "\\"));

			string normalizedBaseDirPath = Path.GetFullPath(WithEnding(baseDirPath.Replace('/', '\\'), "\\"));

			return normalizedPath.StartsWith(normalizedBaseDirPath, StringComparison.OrdinalIgnoreCase);
		}

		/// <summary>
		/// Returns <paramref name="str"/> with the minimal concatenation of <paramref name="ending"/> (starting from end) that
		/// results in satisfying .EndsWith(ending).
		/// </summary>
		/// <example>"hel".WithEnding("llo") returns "hello", which is the result of "hel" + "lo".</example>
		private string WithEnding(string str, string ending)
		{
			if (str == null)
				return ending;

			string result = str;

			// Right() is 1-indexed, so include these cases
			// * Append no characters
			// * Append up to N characters, where N is ending length
			for (int i = 0; i <= ending.Length; i++)
			{
				string tmp = result + Right(ending, i);
				if (tmp.EndsWith(ending))
					return tmp;
			}

			return result;
		}

		/// <summary>Gets the rightmost <paramref name="length" /> characters from a string.</summary>
		/// <param name="value">The string to retrieve the substring from.</param>
		/// <param name="length">The number of characters to retrieve.</param>
		/// <returns>The substring.</returns>
		private string Right(string value, int length)
		{
			if (value == null)
				throw new ArgumentNullException("value");
			if (length < 0)
				throw new ArgumentOutOfRangeException("length", length, "Length is less than zero");

			return (length < value.Length) ? value.Substring(value.Length - length) : value;
		}


		/// <summary>
		/// Checks if a path is incluted with a set reference path.
		/// </summary>
		/// <param name="path">The path should be a subpath of the paths to filter.</param>
		/// <param name="mode"></param>
		public PathInclusionRule(string path, PathSelectionMode mode)
		{
			_referencePath = path;
			_mode = mode;
		}

		public IEnumerable<MetaImage> Process(IEnumerable<MetaImage> imgs)
		{
			foreach(MetaImage img in imgs)
			{
				bool include = IsSubPathOf(img.Path, _referencePath);

				// Inverts the functioning if in blacklist mode.
				if(_mode == PathSelectionMode.Blacklist)
					include = !include;
				if(include)
					yield return img;
			}
			yield break;
		}
	}
}