using System.Collections.Generic;

namespace ImageFinder.Rules
{
	public interface IRule<T>
	{
		IEnumerable<T> Process(IEnumerable<T> objs);
	}
}