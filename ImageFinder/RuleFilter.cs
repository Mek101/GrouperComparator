﻿using System;
using System.Linq;
using System.Collections.Generic;
using ImageFinder.Rules;

namespace ImageFinder
{
	internal class RuleComparator<T> : IComparer<T>
	{
		private readonly List<IRule<T>> _rules;


		public RuleComparator(List<IRule<T>> rules)
		{
			_rules = rules;
		}

		public int Compare(T x, T y)
		{
			throw new System.NotImplementedException();
		}
	}

	public class RuleFilter<T>
	{
		private List<IRule<T>> _rules;


		public IReadOnlyList<IRule<T>> Rules => _rules;


		public RuleFilter()
		{
			_rules = new List<IRule<T>>();
		}

		public RuleFilter(IEnumerable<IRule<T>> rules)
		{
			_rules = new List<IRule<T>>(rules);
		}

		public void AppendRule(IRule<T> rule)
		{
			_rules.Add(rule);
		}

		public void AppendRules(IEnumerable<IRule<T>> rules)
		{
			_rules.AddRange(rules);
		}

		public bool RemoveRule(IRule<T> rule)
		{
			return _rules.Remove(rule);
		}

		/// <summary>
		/// Filters the given source.
		/// </summary>
		/// <param name="source">The elements to filter.</param>
		/// <returns>The approved elements.</returns>
		public IEnumerable<T> Apply(IEnumerable<T> source)
		{
			IEnumerable<T> lastFiltering = source;

			foreach(IRule<T> rule in _rules)
			{
				lastFiltering = rule.Process(lastFiltering);
				if(!lastFiltering.Any())
					return new T[0];
			}
			return lastFiltering;
		}
	}
}
