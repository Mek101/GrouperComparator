using System.Collections.Generic;
using GrouperComparator.Differentiation;
using ImageFinder.Rules;

namespace ImageFinder
{
	public class Configuration
	{
		public List<IRule<MetaImage>> Rules;
		
		public IDifferentiator<MetaImage, double> Differentiator;

		public int Parallelization;
	}
}