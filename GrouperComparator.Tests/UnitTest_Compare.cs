using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;
using QuickGraph;
using GrouperComparator.Differentiation;

namespace GrouperComparator.Tests
{
	using EdgeType = TaggedUndirectedEdge<int, long>;

	internal class Differentiator : IDifferentiator<int, long>
	{
		public long Compare(int elemSource, int elemReference, out bool comparable)
		{
			long diff = Math.Abs(elemSource - elemReference);
			comparable = diff <= 5;
			return diff;
		}
	}

	internal class EdgeComparer : IComparer<EdgeType>
	{
		public static int CompareStatic([AllowNull] EdgeType a, [AllowNull] EdgeType b)
		{
			int tmpDiff = a.Source - b.Source;
			if(tmpDiff == 0)
			{
				// Same source? Compare the targets.
				tmpDiff = a.Target - b.Target;
				if(tmpDiff == 0)
					// Same target? Compare tags. Those are the last fields to compare.
					return (int)Math.Sign(a.Tag - b.Tag);					
			}

			return Math.Sign(tmpDiff);	
		}

		/// <summary>
		/// Orders by Source, then Target, then Tag.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public int Compare([AllowNull] EdgeType a, [AllowNull] EdgeType b)
		{
			return CompareStatic(a, b);
		}
	}

	internal class EdgeEqualityComparer : EqualityComparer<EdgeType>
	{
		public override bool Equals([AllowNull] EdgeType a, [AllowNull] EdgeType b)
		{
			return EdgeComparer.CompareStatic(a, b) == 0;
		}

		public override int GetHashCode([DisallowNull] EdgeType obj)
		{
			return (obj.Source | obj.Target | (int)obj.Tag).GetHashCode();
		}
	}


	public class UnitTest_Compare
	{
		private readonly IDifferentiator<int, long> _differentiator;

		private readonly EdgeComparer _edgeComparer;

		private readonly EdgeEqualityComparer _edgeEqualityComparer;


		private (IEnumerable<EdgeType> Edges, IEnumerable<int> Vertices) BuildExamples(IEnumerable<EdgeType> source)
		{
			AdjacencyGraph<int, EdgeType> exampleGraph = new AdjacencyGraph<int, EdgeType>();
			exampleGraph.AddVerticesAndEdgeRange(source);

			IEnumerable<EdgeType> sortedEdges = exampleGraph.Edges.OrderBy((e) => e, _edgeComparer);
			IEnumerable<int> sortedVertices = exampleGraph.Vertices.OrderBy((v) => v);

			return new ValueTuple<IEnumerable<EdgeType>, IEnumerable<int>>(sortedEdges, sortedVertices);
		}

		private (IEnumerable<EdgeType> Edges, IEnumerable<int> Vertices) ToComparable(AdjacencyGraph<int, EdgeType> source)
		{
			IEnumerable<EdgeType> sortedEdges = source.Edges.OrderBy((e) => e, _edgeComparer);
			IEnumerable<int> sortedVertices = source.Vertices.OrderBy((v) => v);

			return new ValueTuple<IEnumerable<EdgeType>, IEnumerable<int>>(sortedEdges, sortedVertices);
		}

		private void Print<T>(IEnumerable<T> a, IEnumerable<T> b)
		{
			int count = Math.Min(a.Count(), b.Count());

			int exeed = Math.Max(a.Count(), b.Count());
			bool isAExeeder = a.Count() == exeed;

			for(int i = 0; i < count; i++)
			{
				bool equal;
				if(typeof(T) == typeof(EdgeType))
					equal = _edgeComparer.Compare(a.ElementAt(i) as EdgeType, b.ElementAt(i) as EdgeType) == 0;
				else
					equal = a.ElementAt(i).Equals(b.ElementAt(i));

				Console.WriteLine("a = " + a.ElementAt(i) + "\tb = " + b.ElementAt(i) + (equal ? "" : "\tERROR!"));
			}

			if(a.Count() != b.Count())
				for(int i = count; i < exeed; i++)
				{
					if(isAExeeder)
						Console.WriteLine("a = " + a.ElementAt(i));
					else
						Console.WriteLine("\t\tb = " + b.ElementAt(i));
				}
		}


		public UnitTest_Compare()
		{
			_differentiator = new Differentiator();
			_edgeComparer = new EdgeComparer();
			_edgeEqualityComparer = new EdgeEqualityComparer();
		}

		[Fact] public void TestSimpleGraph()
		{
			int[] source = new int[]{ 100, 101, 102 };

			Task<AdjacencyGraph<int, EdgeType>> res = Comparator.GroupInGraphAsync(source, _differentiator);

			var examples = BuildExamples(new EdgeType[]
			{
				new EdgeType(100, 101, 1),
				new EdgeType(101, 100, 1),

				new EdgeType(100, 102, 2),
				new EdgeType(102, 100, 2),

				new EdgeType(101, 102, 1),
				new EdgeType(102, 101, 1)
			});

			var comparableRes = ToComparable(res.Result);
			
			Print(examples.Edges, comparableRes.Edges);
			Print(examples.Vertices, comparableRes.Vertices);


			Assert.Equal(comparableRes.Edges, examples.Edges, _edgeEqualityComparer);
			Assert.Equal(comparableRes.Vertices, examples.Vertices);
		}

		[Fact] public void TestSharedNodes()
		{
			int[] source = new int[]{ 1, 2, 6, 10, 11 };

			Task<AdjacencyGraph<int, EdgeType>> res = Comparator.GroupInGraphAsync(source, _differentiator);

			var examples = BuildExamples(new EdgeType[]
			{
				// The first clique.
				new TaggedUndirectedEdge<int, long>(1, 2, 1),
				new TaggedUndirectedEdge<int, long>(2, 1, 1),

				// The shared node with the first clique.
				new TaggedUndirectedEdge<int, long>(1, 6, 5),
				new TaggedUndirectedEdge<int, long>(6, 1, 5),

				new TaggedUndirectedEdge<int, long>(2, 6, 4),
				new TaggedUndirectedEdge<int, long>(6, 2, 4),

				// The shared node with the second clique.
				new TaggedUndirectedEdge<int, long>(10, 6, 4),
				new TaggedUndirectedEdge<int, long>(6, 10, 4),

				new TaggedUndirectedEdge<int, long>(11, 6, 5),
				new TaggedUndirectedEdge<int, long>(6, 11, 5),

				// The second clique.
				new TaggedUndirectedEdge<int, long>(10, 11, 1),
				new TaggedUndirectedEdge<int, long>(11, 10, 1)
			});

			var comparableRes = ToComparable(res.Result);

			Print(examples.Edges, comparableRes.Edges);
			Print(examples.Vertices, comparableRes.Vertices);


			Assert.Equal(comparableRes.Edges, examples.Edges, _edgeEqualityComparer);
			Assert.Equal(comparableRes.Vertices, examples.Vertices);
		}
	}
}
