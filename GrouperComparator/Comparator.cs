﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using QuickGraph;
using GrouperComparator.Differentiation;
using QuickGraph.Algorithms;

namespace GrouperComparator
{
	internal class SharedThreadContext<V, T>
	{
		private int _currentIndex;

		public IReadOnlyList<V> Source { get; private set; }
		public IDifferentiator<V, T> Differentiator { get; private set; }
		public AdjacencyGraph<V, TaggedUndirectedEdge<V, T>> OutputGraph { get; private set; }

		public SharedThreadContext(IReadOnlyList<V> source, IDifferentiator<V, T> differentiator, AdjacencyGraph<V, TaggedUndirectedEdge<V, T>> outputGraph)
		{
			_currentIndex = -1; // Starting at -1 since the increment operation returns only the incremented value, and we need to match the index with then number of times we need to run the comparation process.
			Source = source;
			Differentiator = differentiator;
			OutputGraph = outputGraph;
		}


		/// <summary>
		/// Gets the next free element index.
		/// </summary>
		/// <returns>The index of the element in source. -1 if the elements have alll been extracted.</returns>
		public int GetNextIndex()
		{
			int i = Interlocked.Increment(ref _currentIndex);
			return i < Source.Count ? i : -1;
		}


		/// <summary>
		/// Adds a new vertice with it's edges to the final output graph.
		/// </summary>
		/// <param name="edges">The edges of the new vertice.</param>
		public void AddVerticesAndEdges(IEnumerable<TaggedUndirectedEdge<V, T>> edges)
		{
			lock (OutputGraph)
				OutputGraph.AddVerticesAndEdgeRange(edges);
		}
	}


	public static class Comparator
	{
		/// <summary>
		/// Concurrently builds the given graph in a locked thread-safe manner.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="boxedArgs"></param>
		private static void BuildGraphThreaded<V, T>(object boxedArgs)
		{
			// Unboxing args...
			SharedThreadContext<V, T> sharedContext = boxedArgs as SharedThreadContext<V, T>;

			// The index of the current vertex candidate.
			int currentIndex;
			while ((currentIndex = sharedContext.GetNextIndex()) != -1)
			{
				IEnumerable<TaggedUndirectedEdge<V, T>> edges = TryGetVerticesAndEdges<V, T>(sharedContext.Source, currentIndex, sharedContext.Differentiator);
				if (edges.Count() != 0)
					sharedContext.AddVerticesAndEdges(edges);
			}
		}

		/// <summary>
		/// Gets all the valid edges to the candidate vertex.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source">The read-only source of elements.</param>
		/// <param name="currentIndex">The index of the current candidate vertex.</param>
		/// <param name="differentiator">Provvides the compare functionality and accepted margin.</param>
		/// <returns>All the found valid edges to teh candidate vertex.</returns>
		private static IEnumerable<TaggedUndirectedEdge<V, T>> TryGetVerticesAndEdges<V, T>(IReadOnlyList<V> source, int currentIndex, IDifferentiator<V, T> differentiator)
		{
			List<TaggedUndirectedEdge<V, T>> foundEdges = new List<TaggedUndirectedEdge<V, T>>();
			V currentElement = source[currentIndex];

			for (int i = currentIndex + 1; i < source.Count; i++)
			{
				T distance = differentiator.Compare(currentElement, source[i], out bool comparable);
				if (comparable)
				{
					foundEdges.Add(new TaggedUndirectedEdge<V, T>(currentElement, source[i], distance));
					foundEdges.Add(new TaggedUndirectedEdge<V, T>(source[i], currentElement, distance)); // The difference is the same in both ways.
				}
			}

			return foundEdges;
		}

		#region SPLITTING // Provides methods to split a graph in sets.
		/// <summary>
		/// Groups the vertex/component-index table pairs into an indexed list of element enumerations, grouping them by the component index.
		/// Each vertex will be assigned to the corresponding index of the returned array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="connectedComponents">The components to regroup.</param>
		/// <param name="componentsCount">The total number of components in the connectedComponents table.</param>
		/// <returns></returns>
		private static Task<IList<IEnumerable<T>>> GroupComponents<T>(IDictionary<T, int> connectedComponents, int componentsCount)
		{
			return Task.Run(delegate()
			{
				IEnumerable<T>[] groupedComponents = new IEnumerable<T>[componentsCount];

				// Each element is collected and put with the others of the same component, which is identified by the index. Done in parallel since percorring an enumeration multiple times can be slow.
				Parallel.For(0, groupedComponents.Length, delegate(int i)
				{
					// Parallel.For provides a copy of the index to the delegate.
					groupedComponents[i] = connectedComponents.Where((p) => p.Value == i).Select((p) => p.Key);
				});

				return (IList<IEnumerable<T>>)groupedComponents;
			});
		}

		/// <summary>
		/// Given a list of vertices, extracts their sub-graph from a given source graph.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceGraphComponent">The vertices which's grpah is to be extracted.</param>
		/// <param name="sourceGraph">The original source graph.</param>
		/// <returns>A graph with only the given vertices.</returns>
		private static IVertexListGraph<V, TaggedUndirectedEdge<V, T>> ToSubGraph<V, T>(IEnumerable<V> sourceGraphComponent, IVertexListGraph<V, TaggedUndirectedEdge<V, T>> sourceGraph)
		{
			AdjacencyGraph<V, TaggedUndirectedEdge<V, T>> subGraph = new AdjacencyGraph<V, TaggedUndirectedEdge<V, T>>();

			foreach (V vertex in sourceGraphComponent)
				// Ignores edges whose target vertices are not present in the given component list.
				foreach(TaggedUndirectedEdge<V, T> edge in sourceGraph.OutEdges(vertex))
					if(sourceGraphComponent.Contains(edge.Target))
						subGraph.AddVerticesAndEdge(edge);
			return subGraph;
		}

		/// <summary>
		/// Extracts the sub-graphs given a table of the source graph components.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceGraphComponents">The vertices of the orignal graph with their component indexes.</param>
		/// <param name="sourceGraph">The source graph containing the vertices and components.</param>
		/// <returns>All the sub-graphs built from the vertex/component-index table.</returns>
		private static Task<IList<IVertexListGraph<V, TaggedUndirectedEdge<V, T>>>> ToSubGraphs<V, T>(IList<IEnumerable<V>> sourceGraphComponents, IVertexListGraph<V, TaggedUndirectedEdge<V, T>> sourceGraph)
		{
			List<IVertexListGraph<V, TaggedUndirectedEdge<V, T>>> subGraphs = new List<IVertexListGraph<V, TaggedUndirectedEdge<V, T>>>(sourceGraphComponents.Count);

			return Task.Run(delegate()
			{
				// Builds the graph by getting the out edges of every vertex in the given component. Parallelized.
				Parallel.For(0, sourceGraphComponents.Count, delegate (int i)
				{
					subGraphs[i] = ToSubGraph(sourceGraphComponents[i], sourceGraph);
				});
				return (IList<IVertexListGraph<V, TaggedUndirectedEdge<V, T>>>)subGraphs;
			});
		}

		/// <summary>
		/// Splits the given graph in indipendendent sub-graphs, using the weakly connected components algorithm to identify the new sub-graphs.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceGraph"></param>
		/// <returns></returns>
		private static async Task<IList<IVertexListGraph<V, TaggedUndirectedEdge<V, T>>>> SplitGraphAsync<V, T>(AdjacencyGraph<V, TaggedUndirectedEdge<V, T>> sourceGraph)
		{
			// Gets the components: the future subgraphs.
			IDictionary<V, int> weaklyConnectedComponents = new Dictionary<V, int>();
			int componentsCount = sourceGraph.WeaklyConnectedComponents(weaklyConnectedComponents);

			// Groups the vertices by components
			IList<IEnumerable<V>> sourceGraphComponents = await GroupComponents(weaklyConnectedComponents, componentsCount);

			// Extracts new subgraphs from the source using the duduced compoents as a base.
			return await ToSubGraphs(sourceGraphComponents, sourceGraph);
		}
		#endregion

		/// <summary>
		/// Organizes a list of elements graph from a list of elements grouping them according to the given differentiator.
		/// </summary>
		/// <typeparam name="T">The type of elements to organize in a graph.</typeparam>
		/// <param name="source">The source of elements to organize in a graph.</param>
		/// <param name="differentiator">The differentiator used to determinate the relationships with the elements.</param>
		/// <param name="compareParallelization">The maximum number of threads to use to organize the graph. No more than the number of available cores or half the elements in the source. Use 0 to run syncronously.</param>
		/// <returns>A bidirectional graph containing the elements organized according to the differentiator. Returns an empy graph with less than two source elements.</returns>
		public static async Task<AdjacencyGraph<V, TaggedUndirectedEdge<V, T>>> GroupInGraphAsync<V, T>(IReadOnlyList<V> source, IDifferentiator<V, T> differentiator, int compareParallelization = int.MaxValue)
		{
			// Preparing and checking parameters.
			if (source == null)
				throw new ArgumentNullException(nameof(source), "The object source of elements cannot be null!");
			if (differentiator == null)
				throw new ArgumentNullException(nameof(differentiator), "The differentiator function and utilities object cannot be null!");

			if (compareParallelization < 0)
				throw new ArgumentOutOfRangeException(nameof(compareParallelization), "The number of threads cannot be bellow 0!");
			if (compareParallelization > source.Count / 2)
				compareParallelization = source.Count / 2;
			if (compareParallelization > Environment.ProcessorCount)
				compareParallelization = Environment.ProcessorCount;

			// Final result.
			AdjacencyGraph<V, TaggedUndirectedEdge<V, T>> outputGraph = new AdjacencyGraph<V, TaggedUndirectedEdge<V, T>>();

			if (source.Count < 2)
				return outputGraph; // Skipping processing for stupid edge-cases.
			else
			{
				// Single or multi threaded asyncronous run, or single threaded syncronous.
				if (compareParallelization > 0)
				{
					// Concurrently builds the graph through worker threads, which then joins to wait for them to end.
					Thread[] threads = new Thread[compareParallelization];

					SharedThreadContext<V, T> sharedContext = new SharedThreadContext<V, T>(source, differentiator, outputGraph);

					for (int i = 0; i < compareParallelization; i++)
					{
						threads[i] = new Thread(BuildGraphThreaded<V, T>);
						threads[i].Start(sharedContext);
					}

					// Awaiting the worker threads to finish...
					await Task.Run(delegate()
					{
						foreach (Thread worker in threads)
							worker.Join();
					});
				}
				else
				{
					for (int i = 0; i < source.Count; i++)
					{
						IEnumerable<TaggedUndirectedEdge<V, T>> edges = TryGetVerticesAndEdges<V, T>(source, i, differentiator);
						if (edges.Count() != 0)
							outputGraph.AddVerticesAndEdgeRange(edges);
					}
				}

				return outputGraph;
			}
		}

		public static async Task<IList<List<V>>> GroupInSetsAsync<V, T>(IReadOnlyList<V> source, IDifferentiator<V, T> differentiator, int compareParallelization = int.MaxValue)
		{
			AdjacencyGraph<V, TaggedUndirectedEdge<V, T>> graph = await GroupInGraphAsync(source, differentiator, compareParallelization);

			IList<IVertexListGraph<V, TaggedUndirectedEdge<V, T>>> subGraphs = await SplitGraphAsync(graph);

			List<V>[] sets = new List<V>[subGraphs.Count];
			Parallel.For(0, subGraphs.Count, delegate(int i)
			{
				sets[i] = subGraphs[i].Vertices.ToList();
			});

			return sets;
		}
	}
}
