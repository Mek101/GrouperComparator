﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuickGraph;
using QuickGraph.Algorithms;
using QuickGraph.Algorithms.KernighanLinAlgoritm;

namespace GrouperComparator
{
//	public static class SetExtractor
//	{
//		/// <summary>
//		/// Groups the vertex/component-index table pairs into an indexed list of element enumerations, grouping them by the component index.
//		/// </summary>
//		/// <typeparam name="T"></typeparam>
//		/// <param name="connectedComponents">The components to regroup.</param>
//		/// <param name="componentsCount">The total number of components in the connectedComponents table.</param>
//		/// <returns></returns>
//		private static Task<IReadOnlyList<IEnumerable<T>>> GroupComponentsAsync<T>(IDictionary<T, int> connectedComponents, int componentsCount)
//		{
//			return Task.Run(delegate ()
//			{
//				IEnumerable<T>[] groupedComponents = new IEnumerable<T>[componentsCount];
//
//				// Each element is collected and put with the others of the same component, which is identified by the index. Done in parallel since percorring an enumeration multiple times can be slow.
//				Parallel.For(0, groupedComponents.Length, delegate (int i)
//				{
//					groupedComponents[i] = connectedComponents.Where((p) => p.Value == i).Select((p) => p.Key);
//				});
//
//				return (IReadOnlyList<IEnumerable<T>>)groupedComponents;
//			});
//		}
//
//
//		/// <summary>
//		/// Extracts a sub-graph from a source graph, given a list of vertices.
//		/// </summary>
//		/// <typeparam name="T"></typeparam>
//		/// <param name="sourceGraphComponent"></param>
//		/// <param name="sourceGraph"></param>
//		/// <returns></returns>
//		private static IVertexListGraph<T, TaggedUndirectedEdge<T, double>> ToSubGraph<T>(IEnumerable<T> sourceGraphComponent, IVertexListGraph<T, TaggedUndirectedEdge<T, double>> sourceGraph)
//		{
//			AdjacencyGraph<T, TaggedUndirectedEdge<T, double>> subGraph = new AdjacencyGraph<T, TaggedUndirectedEdge<T, double>>();
//
//			foreach (T vertex in sourceGraphComponent)
//				// Ignores edges whose target vertices are not present in the given component list.
//				foreach(TaggedUndirectedEdge<T, double> edge in sourceGraph.OutEdges(vertex))
//					if(sourceGraphComponent.Contains(edge.Target))
//						subGraph.AddVerticesAndEdge(edge);
//			return subGraph;
//		}
//
//
//		/// Splitting the graph in subgraphs
//		#region SPLITTING
//		/// <summary>
//		/// Extracts the sub-graphs given a table of the source graph components.
//		/// </summary>
//		/// <typeparam name="T"></typeparam>
//		/// <param name="sourceGraphComponents">The vertices of the orignal graph with their component indexes.</param>
//		/// <param name="sourceGraph">The source graph containing the vertices and components.</param>
//		/// <returns>All the sub-graphs built from the vertex/component-index table.</returns>
//		private static Task<IReadOnlyList<IVertexListGraph<T, TaggedUndirectedEdge<T, double>>>> ToSubGraphsAsync<T>(IReadOnlyList<IEnumerable<T>> sourceGraphComponents, IVertexListGraph<T, TaggedUndirectedEdge<T, double>> sourceGraph)
//		{
//			IVertexListGraph<T, TaggedUndirectedEdge<T, double>>[] subGraphs = new IVertexListGraph<T, TaggedUndirectedEdge<T, double>>[sourceGraphComponents.Count];
//
//			return Task.Run(delegate ()
//			{
//				// Builds the graph by getting the out edges of every vertex in the given component. Parallelized.
//				Parallel.For(0, sourceGraphComponents.Count, delegate (int i)
//				{
//					subGraphs[i] = ToSubGraph(sourceGraphComponents[i], sourceGraph);
//				});
//				return (IReadOnlyList<IVertexListGraph<T, TaggedUndirectedEdge<T, double>>>)subGraphs;
//			});
//		}
//
//
//		/// <summary>
//		/// Splits the given graph in indipendendent sub-graphs, using the weakly connected components algorithm to identify the new sub-graphs
//		/// </summary>
//		/// <typeparam name="T"></typeparam>
//		/// <param name="sourceGraph"></param>
//		/// <returns></returns>
//		private static async Task<IReadOnlyList<IVertexListGraph<T, TaggedUndirectedEdge<T, double>>>> SplitGraphAsync<T>(AdjacencyGraph<T, TaggedUndirectedEdge<T, double>> sourceGraph)
//		{
//			// Gets the components: the future subgraphs.
//			IDictionary<T, int> weaklyConnectedComponents = new Dictionary<T, int>();
//			int componentsCount = sourceGraph.WeaklyConnectedComponents(weaklyConnectedComponents);
//
//			// Groups the vertices by components
//			IReadOnlyList<IEnumerable<T>> sourceGraphComponents = await GroupComponentsAsync(weaklyConnectedComponents, componentsCount);
//
//			// Extracts new subgraphs from the source using the duduced compoents as a base.
//			return await ToSubGraphsAsync(sourceGraphComponents, sourceGraph);
//		}
//		#endregion
//
//
//		#region FINE_ANALYSIS
//		private static Task<Partition<T>> PartitionGraphAsync<T>(IVertexListGraph<T, TaggedUndirectedEdge<T, double>> sourceGraph)
//		{
//			return Task.Run(delegate ()
//			{
//				UndirectedGraph<T, TaggedUndirectedEdge<T, double>> asUndirected = new UndirectedGraph<T, TaggedUndirectedEdge<T, double>>();
//
//				foreach (T vertex in sourceGraph.Vertices)
//				{
//					asUndirected.AddVertex(vertex);
//					foreach (TaggedUndirectedEdge<T, double> edge in sourceGraph.OutEdges(vertex))
//						asUndirected.AddEdge(new TaggedUndirectedEdge<T, double>(edge.Source, edge.Target, edge.Tag));
//				}
//
//				KernighanLinAlgoritm<T, TaggedUndirectedEdge<T, double>> klalg = new KernighanLinAlgoritm<T, TaggedUndirectedEdge<T, double>>(asUndirected, 1);
//				return klalg.Execute();
//			});
//		}
//
//
//		/// <summary>
//		/// Given a graph, extracts all the sets (components) of strongly associated vertices.
//		/// </summary>
//		/// <typeparam name="T"></typeparam>
//		/// <param name="sourceGraph"></param>
//		/// <param name="differentiator">Used to solve conflicts, where a vertex could be part of multiple final components.</param>
//		/// <returns></returns>
//		private static async Task<IReadOnlyList<IEnumerable<T>>> GetSetsAsync<T>(IVertexListGraph<T, TaggedUndirectedEdge<T, double>> sourceGraph)
//		{
//			int componentsCount = sourceGraph.StronglyConnectedComponents(out IDictionary<T, int> stronglyConnectedComponents);
//
//			//if (componentsCount != 0)
//			return await GroupComponentsAsync(stronglyConnectedComponents, componentsCount);
//			//else
//			//{
//			//	// If the whole graph doesn't have any strongly associated component, we must check and eventually partition the graph if there are vertices that could be part of more than one component.
//			//	Partition<T> partitions = await PartitionGraphAsync(sourceGraph);
//
//			//	// Recursively extracts the sets from the partitions.
//			//	List<IEnumerable<T>> results = new List<IEnumerable<T>>();
//			//	results.AddRange(await GetSetsAsync<T>(ToSubGraph(partitions.A, sourceGraph)));
//			//	results.AddRange(await GetSetsAsync<T>(ToSubGraph(partitions.B, sourceGraph)));
//			//	results.TrimExcess();
//			//	return results;
//			//}
//		}
//		#endregion
//
//
//
//		/// <summary>
//		/// Reduces the graph to a serie of sets made of elements related only and fully at each other (Sets of strongly related components)
//		/// </summary>
//		/// <typeparam name="T"></typeparam>
//		/// <param name="sourceGraph"></param>
//		/// <returns></returns>
//		public static async Task<IReadOnlyList<IEnumerable<T>>> SelectSets<T>(AdjacencyGraph<T, TaggedUndirectedEdge<T, double>> sourceGraph)
//		{
//			/*
//			 * Partition at a first level the graph in separated subgraphs through weakly connected elements.
//			 */
//			IEnumerable<IVertexListGraph<T, TaggedUndirectedEdge<T, double>>> subGraphs = await SplitGraphAsync(sourceGraph);
//
//			/*
//			 * Now that we have split the source graph in many indipendent subgraphs, we can do a fine analysis of them in parallel.
//			 */
//			List<IEnumerable<T>> connectedSets = new List<IEnumerable<T>>();
//
//			Parallel.ForEach(subGraphs, async delegate (IVertexListGraph<T, TaggedUndirectedEdge<T, double>> subGraph)
//			{
//				IReadOnlyList<IEnumerable<T>> sets = await GetSetsAsync(subGraph);
//				lock (connectedSets)
//					connectedSets.AddRange(sets);
//			});
//
//			return connectedSets;
//		}
//	}
}
