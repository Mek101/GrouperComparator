﻿
namespace GrouperComparator.Differentiation
{
	public interface IDifferentiator<V, T>
	{
		/// <summary>
		/// Compares the source element against the reference element.
		/// </summary>
		/// <param name="elemSource">Source element.</param>
		/// <param name="elemReference">Reference element.</param>
		/// <param name="comparable">Returns <c>true</c> if the given elements where comparable, <c>false</c> if not.</param>
		/// <returns>The value of the edge connecting the two given vertices.</returns>
		T Compare(V elemSource, V elemReference, out bool comparable);
	}
}
